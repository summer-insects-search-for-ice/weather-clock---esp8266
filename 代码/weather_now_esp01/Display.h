#ifndef __DISPLAY_H__
#define __DISPLAY_H__

#include "WeatherStationFonts.h"
#include "Display.h"


const int I2C_DISPLAY_ADDRESS = 0x3c;  //I2c地址默认
#if defined(ESP8266)
const int SDA_PIN = 0;  //引脚连接
const int SDC_PIN = 2;  //
#endif

//SH1106Wire     display(I2C_DISPLAY_ADDRESS, SDA_PIN, SDC_PIN);   // 1.3寸用这个
SSD1306Wire     display(I2C_DISPLAY_ADDRESS, SDA_PIN, SDC_PIN);   // 0.96寸用这个

//定义当前天气结构体成员
typedef struct CurrentWeatherData {
  String weather_text;     //天气状况
  int weather_code;        //天气代码
  int degree;              //温度
  String dressing;         
  int max_temperature;     //最高温度
  int min_temperature;     //最低温度
  String lastupdate;
}CurrentWeatherData;

const String DAY_NAMES[] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};  //星期
const String MONTH_NAMES[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};  //月份

time_t now;                //实例化时间
/*
struct tm {
   int tm_sec;         // 秒，范围从 0 到 59        
   int tm_min;         // 分，范围从 0 到 59        
   int tm_hour;        // 小时，范围从 0 到 23        
   int tm_mday;        // 一月中的第几天，范围从 1 到 31    
   int tm_mon;         // 月，范围从 0 到 11        
   int tm_year;        // 自 1900 年起的年数        
   int tm_wday;        // 一周中的第几天，范围从 0 到 6    
   int tm_yday;        // 一年中的第几天，范围从 0 到 365    
   int tm_isdst;       // 夏令时                
};
*/
void displaytime()
{
  
  now = time(nullptr);                                                      // 函数的time的返回值为time_t，实际上是一个long数据。此时now存储有年月日时分秒的信息
  struct tm* timeInfo;                                                      // 创建一个tm的结构体指针，该结构体的成员分别是秒、分、时、日、月、年份、星期、一年中的第几天、夏令标识符
  timeInfo = localtime(&now);                                               // localtime的函数的作用是将获取到的long数据类型的数据转化成tm结构体，并存到上一句定义的timeInfo中
  char buff[16];                                                            // 定义一个buff数组
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_CENTER);                             // 设置文字显示方式为居中对齐
  display.setFont(ArialMT_Plain_16);                                       // 设置字体样式及大小
  String date = DAY_NAMES[timeInfo->tm_wday];                               // date为星期几

  sprintf_P(buff, PSTR("%04d-%02d-%02d  %s"), timeInfo->tm_year + 1900, timeInfo->tm_mon + 1, timeInfo->tm_mday, DAY_NAMES[timeInfo->tm_wday].c_str());// 显示日期，星期
  display.drawString(64, 48, String(buff));
  display.setFont(DSEG7_Classic_Regular_24);

  sprintf_P(buff, PSTR("%02d:%02d:%02d"), timeInfo->tm_hour, timeInfo->tm_min, timeInfo->tm_sec);// 显示时分秒
  display.drawString(64, 16 , String(buff));
  display.setTextAlignment(TEXT_ALIGN_LEFT);
/*
  char buff_2[14];                                                          // 页眉绘制
  sprintf_P(buff_2, PSTR("%02d:%02d"), timeInfo->tm_hour, timeInfo->tm_min);

  display.setFont(ArialMT_Plain_10);
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.drawString(6, 54, String(buff_2));
  */
  display.display();
}

//获取天气图标
String getMeteoconIcon(int cond_code) {  //获取天气图标
  if (cond_code == 0 || cond_code == 2) {
    return "B";
  }
  if (cond_code == 99) {
    return ")";
  }
  if (cond_code == 1|| cond_code == 3) {
    return "C";
  }
  if (cond_code == 9) {
    return "D";
  }
  if (cond_code == 500) {
    return "E";
  }
  if (cond_code == 27 || cond_code == 26 || cond_code == 28 || cond_code == 29) {
    return "F";
  }
  if (cond_code == 499 || cond_code == 901) {
    return "G";
  }
  if (cond_code == 5 || cond_code == 6 || cond_code == 7 || cond_code == 8) {//GAI
    return "H";
  }
  if (cond_code == 31 || cond_code == 511 || cond_code == 512 || cond_code == 513) {
    return "L";
  }
  if (cond_code == 30 || cond_code == 509 || cond_code == 510 || cond_code == 514 || cond_code == 515) {
    return "M";
  }
  if (cond_code == 102) {
    return "N";
  }
  if (cond_code == 213) {
    return "O";
  }
  if (cond_code == 11 || cond_code == 303) {
    return "P";
  }
  if (cond_code == 13 || cond_code == 308 || cond_code == 309 || cond_code == 314 || cond_code == 399) {
    return "Q";
  }
  if (cond_code == 14 || cond_code == 15 || cond_code == 16 || cond_code == 17 || cond_code == 18 || cond_code == 315 || cond_code == 316 || cond_code == 317 || cond_code == 318) {
    return "R";
  }
  if (cond_code == 200 || cond_code == 201 || cond_code == 202 || cond_code == 203 || cond_code == 204 || cond_code == 205 || cond_code == 206 || cond_code == 207 || cond_code == 208 || cond_code == 209 || cond_code == 210 || cond_code == 211 || cond_code == 212) {
    return "S";
  }
  if (cond_code == 10 || cond_code == 301) {
    return "T";
  }
  if (cond_code == 22 || cond_code == 408) {
    return "U";
  }
  if (cond_code == 21) {
    return "V";
  }
  if (cond_code == 23 || cond_code == 24 || cond_code == 25 || cond_code == 409 || cond_code == 410) {
    return "W";
  }
  if (cond_code == 12 || cond_code == 19 || cond_code == 20 || cond_code == 405 || cond_code == 406) {
    return "X";
  }
  if (cond_code == 4) {
    return "Y";
  }
  return ")";
}
/*
void displaytime()
{
  now = time(nullptr);                             // 函数time的返回值为time_t，实际上是一个long数据。此时now存储有年月日时分秒的信息                        
  struct tm* timeInfo;                             // 创建一个tm的结构体指针，该结构体的成员分别是秒、分、时、日、月、年份、星期、一年中的第几天、夏令标识符
  timeInfo = localtime(&now);                      // localtime的函数的作用是将获取到的long数据类型的数据转化成tm结构体，并存到上一句定义的timeInfo中
  char buff[16]; 
  char buff2[16];
  String date = DAY_NAMES[timeInfo->tm_wday];
  sprintf_P(buff, PSTR("%04d-%02d-%02d  %s"), timeInfo->tm_year + 1900, timeInfo->tm_mon + 1, timeInfo->tm_mday, DAY_NAMES[timeInfo->tm_wday].c_str());// 显示日期，星期
  sprintf_P(buff2, PSTR("%02d:%02d:%02d"), timeInfo->tm_hour, timeInfo->tm_min, timeInfo->tm_sec);// 显示时分秒
  Serial.println(String(buff));
  Serial.println(String(buff2));
}
*/


#endif
