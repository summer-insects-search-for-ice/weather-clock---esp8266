# 天气时钟-ESP8266

#### 介绍
天气时钟的主控芯片为esp8266，使用0.96寸OLED显示屏。显示时间和天气，通过按键切换显示界面。
![时间信息](https://foruda.gitee.com/images/1666710351530258519/5cc52ed1_8146795.jpeg "1.jpg")
![天气信息](https://foruda.gitee.com/images/1666710371240470229/183a6c0b_8146795.jpeg "4.jpg")
#### 软件架构
代码使用ArduinoIDE编译，需要配置软件的ESP8266开发环境。
天气信息通过调用心知天气API，文末有参考教程，使用该代码需要添加自己的心知天气秘钥。
![添加秘钥](https://foruda.gitee.com/images/1666711075265532283/eb12919a_8146795.png "心知天气秘钥添加.PNG")
硬件上设置了两按键KEY1和KEY2，分别对应IO3和IO1，通过KEY1显示天气信息，包括：
当前气温、天气状况、最高温度和最低温度。
KEY2现在没有任何功能。

#### 硬件说明
| 名称        | 数量 | 规格                             | 备注  |
|-----------|----|--------------------------------|-----|
| ESP8266模块 | 1  | ESP-01S                        |     |
| OLED屏幕    | 1  | IIC，0.96寸                      |     |
| 按键        | 2  | 6*6*5mm，直插式                    |     |
| 电阻        | 2  | 直插电阻，10k                       |     |
| 锂电池充电板    | 1  | TypeC接口                        |     |
| 锂电池       | 1  | 502525-300毫安，3.7v              |     |
| 拨动开关      | 1  | 拨动开关 SS12d00G4 横柄式 二档 三脚 柄长4MM |     |
| 烧录下载器     | 1  | ESP-01S烧录器                     | 非必要 |

其中OLED屏幕的接线顺序为：GND VCC SCL SDA.具体参数可以看硬件清单。
焊接时需要注意元件的焊接顺序，部分元件有相互遮挡要考虑焊接的先后顺序。
PCB板使用立创EDA专业版绘制，PCB开源链接：https://oshwhub.com/xiachongxunbing/esp01s-tian-qi-shi-zhong

#### 使用说明
开机，自动连接设置好的热点。按KEY1切换显示天气信息。

#### 参考资料

程序中需要添加的库文件可以在太极创客的网站和arduino库文件管理器中找到添加
http://www.taichi-maker.com/homepage/download/

太极创客-ESP8266心知天气使用教程
http://www.taichi-maker.com/homepage/iot-development/iot-platform/seniverse/
太极创客-天气时钟粉丝数显示OLED小电视
http://www.taichi-maker.com/oled-weather-time-tv/
ESP8266+OLED桌面气象小电视
https://www.bilibili.com/video/BV1hP4y1x7gL

#### 最后
这个小制作断断续续做了很久，终于想起来画了PCB，整理资料。后面可能还会改进PCB和制作外壳，丰富程序功能。

